<?php

/**
 * @file
 * Added cURL functionality for Paddle.
 */

/**
 * Checks if the cURL connection failed or not.
 *
 * @param resource $curl_object
 *   The object created through cURL.
 * @param string $message
 *   The error messages which needs to be sent.
 * @param string $send_mail
 *   Whether you want to send a mail if an error occurs.
 * @param bool $is_form
 *   Whether a form caused the cURL connection.
 * @param string $form_item
 *   The form element.
 *
 * @return bool
 *   Whether the cURL connection failed or not.
 */
function paddle_core_check_for_curl_errors($curl_object, $message, $send_mail = FALSE, $is_form = FALSE, $form_item = '') {
  $has_errors = FALSE;
  $successful_http_codes = array(200, 201, 202, 203, 204, 205, 206);
  if (curl_error($curl_object)) {
    // Log the error message.
    $message_parameters = array(
      '@code' => curl_errno($curl_object),
      '@error' => curl_error($curl_object),
      '@url' => curl_getinfo($curl_object, CURLINFO_EFFECTIVE_URL),
    );
    watchdog('curl_connection', $message, $message_parameters);
    drupal_set_message(t($message, $message_parameters), 'error');

    if ($is_form && !empty($form_item)) {
      form_set_error($form_item, t($message, $message_parameters));
    }

    if ($send_mail) {
      paddle_core_send_mail_to_support($message, $message_parameters);
    }

    $has_errors = TRUE;
  }
  elseif (!in_array($status_code = curl_getinfo($curl_object, CURLINFO_HTTP_CODE), $successful_http_codes)) {
    $status_text = 'HTTP error while connecting to the server using url @url: @status_code';
    $status_parameters = array(
      '@status_code' => $status_code,
      '@url' => curl_getinfo($curl_object, CURLINFO_EFFECTIVE_URL),
    );
    watchdog('curl_connection', $status_text, $status_parameters);
    if ($is_form && !empty($form_item)) {
      form_set_error($form_item, t($status_text, $status_parameters));
    }
    else {
      drupal_set_message(t($status_text, $status_parameters), 'error');
    }

    if ($send_mail) {
      paddle_core_send_mail_to_support($status_text, $status_parameters);
    }

    $has_errors = TRUE;
  }

  return $has_errors;
}

/**
 * Sends a mail to Paddle Support when an issue occurred during a cURL action.
 *
 * @param string $message
 *   The error which has to be send.
 * @param string $message_parameters
 *   The variables of the message.
 */
function paddle_core_send_mail_to_support($message, $message_parameters) {
  $support_address = variable_get('paddle_core_support', 'support@paddle.be');

  $params = array();
  $params['subject'] = t('[@site_name] - An error occurred while sending a request to a remote server', array(
    '@site_name' => variable_get('site_name', 'Drupal'),
  ));

  $params['message'] = "Dear member of the Paddle Support Team,<br />";
  $params['message'] .= "<p>An issue occurred on @site_name</p>";
  $params['message'] .= "<p>The following error occurred: @message</p>";
  $params['message'] .= "<p>This is an automatically generated message. Please do not reply to the sender of this message.</p>";
  $params['message'] = t($params['message'], array(
    '@site_name' => variable_get('site_name', 'Drupal'),
    '@message' => t($message, $message_parameters),
  ));
  $params['headers'] = array(
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/html; charset=UTF-8; format=flowed',
    'Content-Transfer-Encoding' => '8Bit',
    'X-Mailer' => 'Drupal',
  );
  $params['plaintext'] = NULL;
  $params['plain'] = FALSE;

  drupal_mail('paddle_core', 'paddle_curl', $support_address, language_default(), $params, variable_get('site_mail'));

}
