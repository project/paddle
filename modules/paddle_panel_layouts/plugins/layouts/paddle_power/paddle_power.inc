<?php

/**
 * @file
 * Declare the settings for the power layout.
 */

/**
 * Plugin definition for the power layout.
 */
$plugin = array(
  // Displayed title of the layout.
  'title' => t('Power'),
  // Category of the layout, if we want to add a layout for Landing pages, do pick the 'Paddle Layouts' category.
  'category' => t('Paddle Layouts'),
  // Icon which is displayed to pick your layout in the GUI.
  'icon' => 'paddle_power.jpg',
  // The (unique) machine name of your plugin.
  'theme' => 'paddle_power',
  // The (empty) CSS file of your plugin
  'css' => 'paddle_power.css',
  // The regions within your layout, keyed by machine name and the translatable label as value,
  // by default we follow the letters of the alphabet to display our regions.
  'regions' => array(
    '1_a' => t('A'),
    '2_b' => t('B'),
    '2_c' => t('C'),
    '2_d' => t('D'),
    '3_e' => t('E'),
    '4_f' => t('F'),
    '4_g' => t('G'),
    '5_h' => t('H'),
    '6_i' => t('I'),
    '6_j' => t('J'),
    '6_k' => t('k'),
    '7_l' => t('L'),
    '8_m' => t('M'),
    '8_n' => t('N'),
    '9_o' => t('O')
  ),
);
