<?php

/**
 * @file
 * Template for the speed layout.
 *
 * Variables:
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following:
 * - A top row with full width
 * - A second row with three sections, each of 1/3 width of the row
 * - A third row with full width
 * - A fourth row with two sections, being 2/3 and 1/3 widht of the row respectively
 * - A fifth row with full width
 * - A sixth row with three sections, each of 1/3 width of the row
 * - A seventh row with full width
 * - A eight row with two sections, being 2/3 and 1/3 widht of the row respectively
 * - A bottom row with full width
 */
?>
<div class="container paddle-layout-paddle_power <?php print $classes; ?>">
  <div class="row">
    <div class="col-xs-12">
      <?php print $content['1_a']; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <?php print $content['2_b'] ?>
    </div>
    <div class="col-md-4">
      <?php print $content['2_c'] ?>
    </div>
    <div class="col-md-4">
      <?php print $content['2_d'] ?>
    </div>
  </div>
  <div class="row">
    <div class=col-xs-12">
      <?php print $content['3_e']; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      <?php print $content['4_f']; ?>
    </div>
    <div class="col-md-4">
      <?php print $content['4_g']; ?>
    </div>
  </div>
<div class="row">
  <div class="col-xs-12">
    <?php print $content['5_h']; ?>
  </div>
</div>
  <div class="row">
    <div class="col-md-4">
      <?php print $content['6_i'] ?>
    </div>
    <div class="col-md-4">
      <?php print $content['6_j'] ?>
    </div>
    <div class="col-md-4">
      <?php print $content['6_k'] ?>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <?php print $content['7_l']; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      <?php print $content['8_m']; ?>
    </div>
    <div class="col-md-4">
      <?php print $content['8_n']; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <?php print $content['9_o']; ?>
    </div>
  </div>
</div>
