<?php

/**
 * @file
 * Declare the settings for the speed layout.
 */

/**
 * Plugin definition for the speed layout.
 */
$plugin = array(
  // Displayed title of the layout.
  'title' => t('Speed'),
  // Category of the layout, if we want to add a layout for Landing pages, do pick the 'Paddle Layouts' category.
  'category' => t('Paddle Layouts'),
  // Icon which is displayed to pick your layout in the GUI.
  'icon' => 'paddle_speed.jpg',
  // The (unique) machine name of your plugin.
  'theme' => 'paddle_speed',
  // The (empty) CSS file of your plugin
  'css' => 'paddle_speed.css',
  // The regions within your layout, keyed by machine name and the translatable label as value,
  // by default we follow the letters of the alphabet to display our regions.
  'regions' => array(
    '1_a' => t('A'),
    '1_b' => t('B'),
    '2_c' => t('C'),
    '2_d' => t('D'),
    '2_e' => t('E'),
    '3_f' => t('F'),
    '3_g' => t('G'),
    '3_h' => t('H'),
    '4_i' => t('I'),
    '4_j' => t('J')
  ),
);
