<?php

/**
 * @file
 * Template for the speed layout.
 *
 * Variables:
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following:
 * - A top row with two sections, being 1/3 and 2/3 of the width of the row
 * - A second row with three sections, each of 1/3 width of the row
 * - A third row with three sections, each of 1/3 width of the row
 * - A bottom row with two section, being 2/3 and 1/3 width of the row
 */
?>
<div class="container paddle-layout-paddle_speed <?php print $classes; ?>">
  <div class="row">
    <div class="col-lg-4 col-sm-6">
      <?php print $content['1_a']; ?>
    </div>
    <div class="col-lg-8 col-sm-6">
      <?php print $content['1_b']; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <?php print $content['2_c'] ?>
    </div>
    <div class="col-sm-4">
      <?php print $content['2_d'] ?>
    </div>
    <div class="col-sm-4">
      <?php print $content['2_e'] ?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <?php print $content['3_f'] ?>
    </div>
    <div class="col-sm-4">
      <?php print $content['3_g'] ?>
    </div>
    <div class="col-sm-4">
      <?php print $content['3_h'] ?>
    </div>
  </div>

  <div class="row">
    <div class="col-md-8 col-sm-6">
      <?php print $content['4_i']; ?>
    </div>
    <div class="col-md-4 col-sm-6">
      <?php print $content['4_j']; ?>
    </div>
  </div>
</div>
