<?php

/**
 * @file
 * Declare the settings for the education layout.
 */

/**
 * Plugin definition for the phi layout.
 */
$plugin = array(
  // Displayed title of the layout.
  'title' => t('Education'),
  // Category of the layout, if we want to add a layout for Landing pages, do pick the 'Paddle Layouts' category.
  'category' => t('Paddle Layouts'),
  // Icon which is displayed to pick your layout in the GUI.
  'icon' => 'paddle_education.png',
  // The (unique) machine name of your plugin.
  'theme' => 'paddle_education',
  // The (empty) CSS file of your plugin
  'css' => 'paddle_education.css',
  // The regions within your layout, keyed by machine name and the translatable label as value,
  // by default we follow the letters of the alphabet to display our regions.
  'regions' => array(
    'full_a' => t('A'),
    'nested_9_b' => t('B'),
    'nested_3_c' => t('C'),
    'full_d' => t('D')
  ),
);
