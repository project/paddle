<?php

/**
 * @file
 * Template for the education layout.
 *
 * Variables:
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following:
 * - A top row which only provides one section, being the full width of the row
 * - A middle row in which we provide two sections, being 3.4 and 1/4 of the width of the row
 * - A bottom row which only provides one section, being the full width of the row.
 */
?>
<div class="row paddle-layout-paddle_education <?php print $classes; ?>">
  <div class="row">
    <div class="col-md-12">
      <?php print $content['full_a']; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-lg-9">
      <?php print $content['nested_9_b'] ?>
    </div>
    <div class="col-md-12 col-lg-3">
      <?php print $content['nested_3_c'] ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <?php print $content['full_d']; ?>
    </div>
  </div>
</div>
