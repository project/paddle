<?php

/**
 * @file
 * Search API StopWords alteration.
 */

/**
 * Processor for removing stopwords from index and search terms.
 */
class PaddleSearchStopWords extends SearchApiStopWords {

  public function process(&$value) {
    if (!empty(variable_get('paddle_search_filter_stopwords_enabled', TRUE))) {
      parent::process($value);
    }
  }
}